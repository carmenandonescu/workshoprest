package com.endava.rest.controller;

import com.endava.rest.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequestMapping("/employees")
@RestController
public class EmployeeController {

    private static final List<Employee> employees = new ArrayList<>() {{
        add(new Employee("1", "Ion", "Ionescu"));
        add(new Employee("2", "Marcel", "Marculescu"));
        add(new Employee("3", "Vasile", "Vasilescu"));
        add(new Employee("4", "Gheorghe", "Georgescu"));
        add(new Employee("5", "Vicentiu", "Vladimirescu"));
    }};

    @RequestMapping
    public List<Employee> collection(@RequestParam(name = "firstName", required = false) String name) {
        if (name != null) {
            return findEmployees(name);
        }
        return employees;
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Employee> byId(@PathVariable(value = "id") String id) {
        return findOptionalEmployee(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        employees.add(employee);
        return new ResponseEntity<>(employee, HttpStatus.CREATED);
    }

    private List<Employee> findEmployees(String name) {
        return EmployeeController.employees.stream()
                .filter(employee -> employee.getFirstName().equals(name))
                .collect(Collectors.toList());
    }

    private ResponseEntity<Employee> findOptionalEmployee(String id) {
        return Optional.of(EmployeeController.employees.stream()
                .filter(employee -> employee.getId().equals(id)).findFirst()
                .map(employee -> ResponseEntity.ok().body(employee))
                .orElseGet(() -> ResponseEntity.notFound().build()))
                .get();
    }

    private Employee findEmployee(List<Employee> employees, String id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findAny()
                .orElse(null);
    }
}

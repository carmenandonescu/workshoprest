package com.endava.rest.controller;

import com.endava.rest.controller.general.Error;
import com.endava.rest.controller.general.Validation;
import com.endava.rest.model.Movie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/movies")
public class MovieController {

    public static final List<Movie> movies = new ArrayList<>() {{
        add(new Movie("1", "Pulp Fiction", LocalDate.of(1994, 10, 14), "Quentin Tarantino"));
        add(new Movie("2", "The Shawshank Redemption", LocalDate.of(1994, 10, 14), "Frank Darabont"));
        add(new Movie("3", "The Godfather", LocalDate.of(1972, 3, 24), "Francis Ford Coppola"));
        add(new Movie("4", "The Dark Knight", LocalDate.of(2008, 7, 18), "Christopher Nolan"));
        add(new Movie("5", "Schindler's List", LocalDate.of(1994, 2, 4), "Steven Spielberg"));
        add(new Movie("6", "The Good, the Bad and the Ugly ", LocalDate.of(1967, 12, 29), "Sergio Leone"));
    }};

    @GetMapping
    public ResponseEntity<List<Movie>> getMovies(@RequestParam(name = "name", required = false) String name) {
        if (name != null) {
            return ResponseEntity.ok(findMovies(name));
        }
        return ResponseEntity.ok(movies);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Movie> byId(@PathVariable(value = "id") String id) {
        return findMovie(id);
    }

    @PostMapping()
    public ResponseEntity createMovie(@RequestBody Movie movie) {
        Optional<Movie> existingMovie = getExistingMovie(movie);

        if (existingMovie.isPresent()) {
            return ResponseEntity.badRequest().body(Error.DUPLICATE_ID);
        } else if (!Validation.isReleaseDateCorrect(movie.getReleaseDate())) {

            return ResponseEntity.badRequest().body(Error.RELEASE_DATE);
        } else {

            movies.add(movie);
            return ResponseEntity
                    .created(URI.create("/movies/" + movie.getId())).body(movie);

        }

    }

    @PutMapping("/{id}")
    public ResponseEntity updateMovie(@PathVariable(value = "id") String id, @RequestBody Movie update) {
        Optional<Movie> movieOpt = movies.stream().filter(mov -> mov.getId().equals(id)).findFirst();
        if (movieOpt.isEmpty()) {
            return ResponseEntity.badRequest().body(Error.NO_ENTRY);
        }

        Movie movieToUpdate = movieOpt.get();
        movieToUpdate.setName(update.getName());
        movieToUpdate.setDirector(update.getDirector());
        movieToUpdate.setReleaseDate(update.getReleaseDate());

        return ResponseEntity.ok(movieToUpdate);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteMovie(@PathVariable(value = "id") String id) {

        return movies.removeIf(movie -> String.valueOf(movie.getId()).equals(id))
                ? ResponseEntity.ok("The movie has been successfully deleted!")
                : ResponseEntity.notFound().build();
    }

    private Optional<Movie> getExistingMovie(Movie newMovie) {
        return movies.stream()
                .filter(movie -> movie.getId().equals(newMovie.getId()))
                .findAny();
    }

    private ResponseEntity<Movie> findMovie(String id) {
        return Optional.of(MovieController.movies.stream()
                .filter(movie -> String.valueOf(movie.getId()).equals(id))
                .findFirst()
                .map(movie -> ResponseEntity.ok().body(movie))
                .orElseGet(() -> ResponseEntity.notFound().build())).get();
    }

    private List<Movie> findMovies(String name) {
        return MovieController.movies.stream()
                .filter(movie -> movie.getName().equals(name))
                .collect(Collectors.toList());
    }
}
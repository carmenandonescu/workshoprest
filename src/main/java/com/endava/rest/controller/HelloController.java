package com.endava.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/greeting")
    public String helloWorld(@RequestParam(name="greetName", defaultValue="John Doe") String name,
                             @RequestParam(name = "greetUser", required = false) String user) {
        return "hello " + name;
    }
}


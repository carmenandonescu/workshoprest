package com.endava.rest.controller.general;

public class Error {

    public static final String DUPLICATE_ID = "The id is already present!";
    public static final String NO_ENTRY = "There is no entry with the ID you've requested!";
    public static final String RELEASE_DATE = "Movie's release date must be less than the current date!";
}
package com.endava.rest.controller.general;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;

public class Validation {

    public static boolean isReleaseDateCorrect (LocalDate releaseDate){
        return releaseDate.isBefore(ChronoLocalDate.from(LocalDate.now()));

    }
}
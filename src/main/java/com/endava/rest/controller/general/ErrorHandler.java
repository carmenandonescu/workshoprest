package com.endava.rest.controller.general;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Data
@NoArgsConstructor
public class ErrorHandler implements ResponseErrorHandler {

    private int rawStatusCode;
    private String body;

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return new DefaultResponseErrorHandler().hasError(response);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        switch (HttpStatus.Series.valueOf(response.getStatusCode().series().name())) {
            case SERVER_ERROR:
                rawStatusCode = response.getRawStatusCode();
                break;

            default:
                rawStatusCode = response.getRawStatusCode();
                body = response.getBody().toString();
        }
    }
}
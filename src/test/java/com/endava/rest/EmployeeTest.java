package com.endava.rest;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.CoreMatchers.is;

public class EmployeeTest {
    private final static String HOSTNAME = "http://localhost:8080";

    @Test
    public void shouldReceiveHelloWorld() {
        RestTemplate restTemplate = new RestTemplate();
        String message = restTemplate.getForObject(HOSTNAME + "/greeting?greetName=World", String.class);
        Assert.assertThat(message, is("hello World"));
    }

}

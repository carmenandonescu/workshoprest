package com.endava.rest;

import com.endava.rest.controller.MovieController;
import com.endava.rest.controller.general.Error;
import com.endava.rest.controller.general.ErrorHandler;
import com.endava.rest.model.Movie;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.*;

public class MovieTest {

    private final static String URL = "http://localhost:8080/movies/";
    final List<Movie> movies = MovieController.movies;
    final RestTemplate restTemplate = new RestTemplate();
    final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void count() {
        //given when
        ResponseEntity<Movie[]> response = restTemplate.getForEntity(URL, Movie[].class);
        //then
        Assert.assertThat(Objects.requireNonNull(response.getBody()).length, is(movies.size()));
    }

    @Test
    public void getAll() {
        //given when
        ResponseEntity<Movie[]> response = restTemplate.getForEntity(URL, Movie[].class);
        //then
        Assert.assertArrayEquals(response.getBody(), movies.toArray());
    }

    @Test
    public void getByName() {
        //given
        Movie expectedMovie = new Movie("1", "Pulp Fiction", LocalDate.of(1994, 10, 14),
                "Quentin Tarantino");
        //when
        ResponseEntity<Movie[]> response = restTemplate.getForEntity(URL + "?name=" + expectedMovie.getName(), Movie[].class);
        Movie[] movies = response.getBody();
        //then
        assert movies != null;
        Assert.assertThat(movies[0], equalTo(expectedMovie));
    }

    @Test
    public void getById() {
        //given
        Movie expectedMovie = new Movie("1", "Pulp Fiction", LocalDate.of(1994, 10, 14),
                "Quentin Tarantino");
        //when
        Movie movie = restTemplate.getForObject(URL + expectedMovie.getId(), Movie.class);
        //then
        assert movie != null;
        Assert.assertThat(movie.toString(), equalTo(expectedMovie.toString()));
    }

    @Test
    public void add() throws JsonProcessingException {
        //given
        Movie expectedMovie = new Movie("7", "Godfather II", LocalDate.of(1974, 12, 18),
                "Francis Ford Coppola");
        //when
        ResponseEntity<String> response = restTemplate.postForEntity(URL, expectedMovie, String.class);
        Movie actualMovie = objectMapper.readValue(Objects.requireNonNull(response.getBody()), Movie.class);
        //then
        Assert.assertThat(response, notNullValue());
        Assert.assertThat(response.getStatusCodeValue(), is(201));
        Assert.assertThat(actualMovie, equalTo(expectedMovie));
        //reset
        restTemplate.delete(URL + "7", expectedMovie, String.class);
    }

    @Test
    public void addExistingId() {

        restTemplate.setErrorHandler(new ErrorHandler());
        //given
        Movie expectedMovie = new Movie("7", "Godfather II", LocalDate.of(1974, 12, 18),
                "Francis Ford Coppola");
        //when
        restTemplate.postForEntity(URL, expectedMovie, String.class);
        ResponseEntity<String> response = restTemplate.postForEntity(URL, expectedMovie, String.class);
        //then
        Assert.assertThat(response.getStatusCodeValue(), is(400));
        Assert.assertThat(response.getBody(), is(Error.DUPLICATE_ID));
        //reset
        restTemplate.delete(URL + "7", expectedMovie, String.class);
    }

    @Test
    public void addReleaseDateInTheFuture() {

        restTemplate.setErrorHandler(new ErrorHandler());
        //given
        Movie expectedMovie = new Movie("7", "Godfather II", LocalDate.of(2030, 12, 18),
                "Francis Ford Coppola");
        //when
        restTemplate.postForEntity(URL, expectedMovie, String.class);
        ResponseEntity<String> response = restTemplate.postForEntity(URL, expectedMovie, String.class);
        //then
        Assert.assertThat(response.getStatusCodeValue(), is(400));
        Assert.assertThat(response.getBody(), is(Error.RELEASE_DATE));
    }

    @Test
    public void update() {
        //given
        String id = "1";
        Movie movie = restTemplate.getForObject(URL + id, Movie.class);
        movie.setDirector("Quentin Tarantino Senior");
        //when
        restTemplate.put(URL + id, movie, String.class);
        //then
        Movie updatedMovie = restTemplate.getForObject(URL + id, Movie.class);
        Assert.assertThat(updatedMovie.getDirector(), is(movie.getDirector()));
        //reset
        updatedMovie.setDirector("Quentin Tarantino");
        restTemplate.put(URL + id, updatedMovie, String.class);

    }

    @Test
    public void delete() {

        String movieId = "7";
        Movie newestMovie = new Movie(movieId, "The Godfather II", LocalDate.of(1974, 12, 18),
                "Francis Ford Copolla");
        restTemplate.postForEntity(URL, newestMovie, String.class);
        //given
        ResponseEntity<Movie[]> firstResponse = restTemplate.getForEntity(URL, Movie[].class);
        int allMovies = Objects.requireNonNull(firstResponse.getBody()).length;
        Movie movieToBeDeleted = restTemplate.getForObject(URL + newestMovie.getId(), Movie.class);
        //when
        restTemplate.delete(URL + movieId, movieToBeDeleted, String.class);
        //then
        ResponseEntity<Movie[]> secondResponse = restTemplate.getForEntity(URL, Movie[].class);
        Assert.assertThat(Objects.requireNonNull(secondResponse.getBody()).length, is(allMovies - 1));
    }
}